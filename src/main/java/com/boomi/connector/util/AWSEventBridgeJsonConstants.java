/*
*  Copyright (c) 2020 Boomi, Inc.
*/
package com.boomi.connector.util;

/**
 * 
 * 
 * @author a.kumar.samantaray
 *
 */
public class AWSEventBridgeJsonConstants {
	private AWSEventBridgeJsonConstants() {

	}

	public static final String ENTRIES = "Entries";
	public static final String EVENTID = "EventId";
	public static final String SOURCE = "Source";
	public static final String DETAIL = "Detail";
	public static final String DETAILTYPE = "DetailType";
	public static final String RESOURCES = "Resources";
	public static final String EVENTBUSNAME = "EventBusName";
}
